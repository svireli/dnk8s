﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DNK8S.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DebugController : ControllerBase
    {
        private ILogger _logger;
        private IConfiguration _configuration;
        public DebugController(ILogger<DebugController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public string Get()
        {
            StringBuilder stringBuilder = new StringBuilder();
            string configurationString = ((IConfigurationRoot)_configuration).GetDebugView().ToString();
            stringBuilder.Append(configurationString);
            stringBuilder.Append("\n" + Dns.GetHostName());
            return stringBuilder.ToString();
        }
}
}
